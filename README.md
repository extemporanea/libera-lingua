# Libera lingua

Discutiamo di lingua, scienza e società.


## Lingua e questioni di genere

- Abbiamo cominciato una conversazione sulle questioni di genere nella lingua, coordinata da @elenalma. Essa ha luogo su questo documento collaborativo: https://demo.hedgedoc.org/nP016MSBSDqYilL0w7pX7g


- @elenalma  ha anche contribuito un documento pdf che fa la sintesi del suo punto di vista sulla question. Lo potete trovare in [documenti/Contributi sul linguaggio inclusivo/Elena Alma Rastello e Elia Faso.pdf](documenti/Contributi sul linguaggio inclusivo/Elena Alma Rastello e Elia Faso.pdf) con una risposta di @mattiagaleotti

- Abbiamo costituito un gruppo Zotero per raccogliere una bibliografia rilevante di libri, siti web e documenti vari: https://www.zotero.org/groups/2769808/libera_lingua/

- Qualche link utile alla discussione: 
   1. Dialogo tra Andrea Moro e Vera Gheno a Giovedìscienza: https://www.giovediscienza.it/it/appuntamenti/520-quando-la-parola-conta-pi-dei-fatti
   2. Intervista di Andrea Moro a Noam Chomsky a Festivaletteratura 2020: https://www.youtube.com/watch?v=Pc9VURRZ3ZU&ab_channel=Festivaletteratura
   3. Intervista a Noam Chomsky su lingua, scienza e società: https://www.youtube.com/watch?v=pUWmTXkpHjE&t=15s&ab_channel=TheoriesofEverythingwithCurtJaimungal
